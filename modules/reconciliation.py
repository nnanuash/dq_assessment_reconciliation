"""
reconciliation.py
====================================
**Functions specifically for reconciliation**


Packages that are used for this module
======================================  

.. code-block:: python

    import pandas as pd
    from pandas import testing
    import numpy as np
    from typing import List

"""
import pandas as pd
import numpy as np
from typing import List


def match_columns(data: pd.DataFrame, source_name: str) -> pd.DataFrame:
    """ Keep only the columns in the target that come from the given source. 
    
    It can be done by identifying these columns with the source table name as a prefix in their names.

    Args:
        data (pd.DataFrame): Target table - data model
        source_name (str): source table name with the underscore like sourceName and underscore "_"

    Returns:
        pd.DataFrame: data set with only the columns from a certain source ::
            
            def match_columns(data: pd.DataFrame, source_name: str) -> pd.DataFrame:
            
            # iterate over column names to keep only the columns that come from the given source
            col_list = [col for col in data.columns if col.startswith(source_name)]
            data_out = data[col_list]

            return data_out

    """

    # iterate over column names to keep only the columns that come from the given source
    col_list = [col for col in data.columns if col.startswith(source_name)]
    data_out = data[col_list]

    return data_out


def filter_columns(data1: pd.DataFrame, data2: pd.DataFrame) -> pd.DataFrame:
    """Select columns from the data1 that are also in the data2. In other words filter data1 based on the column list from data2.

        Print out the rest of the column names that are not shared by both data sets.

    Args:
        data1 (pd.DataFrame): data set to be filtered.
        data2 (pd.DataFrame): data set that the filter is based on.

    Returns:
        pd.DataFrame: smaller version of data1 that has only the columns also present in data2. ::

            def filter_columns(data1:pd.DataFrame, data2:pd.DataFrame) -> pd.DataFrame:
                
                # match the names of source and model 
                # only select columns from the source that are also in the model
                data_out = data1[data2.columns]
                
                # print out the names of the columns that are in the source but not in the model
                columns_rest = data1.columns[ ~ data1.columns.isin(data2.columns)]
                print(columns_rest)

                # assert that the column number are the same in both data sets
                try:
                    assert(data2.shape[1] == data_out.shape[1])
                finally:
                    return data_out  

    """

    # match the names of source and model - only select columns from the source that are also in the model
    data_out = data1[data2.columns]
    # print out the names of the columns that are in the source but not in the model
    columns_rest = data1.columns[~data1.columns.isin(data2.columns)]
    print(columns_rest)

    # assert that the column number are the same in both data sets
    try:
        assert data2.shape[1] == data_out.shape[1]
    finally:
        return data_out


def semi_join(
    data1: pd.DataFrame, data2: pd.DataFrame, key_column_name: List[object]
) -> pd.DataFrame:
    """
    Perform semi-join to filter the source data set based on the target (data model). Only the records that are present in the target are left in the source and these data sets become comparable using python package `datacompy` or any other method.

    Args:

        data1 (pd.DataFrame): left data frame (in this case it must be the source data set)
        data2 (pd.DataFrame): right data frame the target (in this case - DM)
        key_column_name (List[object]): key identifier column name(s) to make the join on.can be multiple columns as a list of names
        
    Returns:
        pd.DataFrame: data1 - source with only the records that correspond to key identifiers in the data2 - target. ::
       
            def semi_join(
                data1: pd.DataFrame, data2: pd.DataFrame, key_column_name: List[object]
            ) -> pd.DataFrame:

            # inner join first
                temp = data1.merge(data2, on=key_column_name)
                if len(key_column_name) > 1:
                    # then semi join
                    result = data1[data1[key_column_name].agg(
                        tuple, 1).isin(temp[key_column_name].agg(tuple, 1))]
                else:
                    result = data1[data1[key_column_name].isin(temp[key_column_name])]

                # return resulted data frame
                return result

    """

    # inner join first
    temp = data1.merge(data2, on=key_column_name)
    if len(key_column_name) > 1:
        # then semi join
        result = data1[
            data1[key_column_name]
            .agg(tuple, 1)
            .isin(temp[key_column_name].agg(tuple, 1))
        ]
    else:
        result = data1[data1[key_column_name].isin(temp[key_column_name])]

    # return resulted data frame
    return result


def test_2_data_are_equal(data1: pd.DataFrame, data2: pd.DataFrame, index: List[str]):
    """Check if 2 data sets compare as equal, ignoring data types. It can be done if the record counts in data set are equal. 
    
    To make data sets comparable, set the key identifier column(s) as DataFrame index first and sort it in both data sets.


    Args:
        data1 (pd.DataFrame): data set 1
        data2 (pd.DataFrame): data set 2
        index (List[str]): column name(s) as a string or list of strings. ::
        
            def test_2_data_are_equal(data1: pd.DataFrame, data2: pd.DataFrame, index: str):
            
                # set index to key identifier and sort it in both data sets
                data1_sorted = data1.set_index(index).sort_index()
                data2_sorted = data2.set_index(index).sort_index()

                try:
                    # with pandas testing check if the rest of the values are the same, ignoring data types
                    pd.testing.assert_frame_equal(
                        data1_sorted, data2_sorted, check_dtype=False, check_index_type=False,
                    )
                finally:    

                    # check again the equality of data sets
                    is_true = data1_sorted.equals(data2_sorted)
                    
                    if is_true:
                        print("Target reconciles with the source.")
                    else:
                        print("Target does not reconciles with the source.")

    """
    # set index to key identifier and sort it in both data sets
    data1_sorted = data1.set_index(index).sort_index()
    data2_sorted = data2.set_index(index).sort_index()

    try:
        # with pandas testing check if the rest of the values are the same, ignoring data types
        pd.testing.assert_frame_equal(
            data1_sorted, data2_sorted, check_dtype=False, check_index_type=False,
        )
    finally:

        # check again the equality of data sets
        is_true = data1_sorted.equals(data2_sorted)

        if is_true:
            print("Target reconciles with the source.")
        else:
            print("Target does not reconciles with the source.")

