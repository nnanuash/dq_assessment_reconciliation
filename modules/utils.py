"""
utils.py
====================================
**The core module of DQ assessment** 

The module contains functions created specifically to perform data quality assessment as well as more general functions, for example, for loading data.



Packages that are used for this module
======================================  

    .. code-block:: python

        
       from pandas.core.series import Series
       import sqlalchemy
       from sqlalchemy import create_engine
       import pandas as pd
       import numpy as np
       from pandas.core.frame import DataFrame
       import seaborn as sns
       import matplotlib.pyplot as plt
       import re
       import json
"""

from typing import List
from pandas.core.series import Series
import sqlalchemy
from sqlalchemy import create_engine
import pandas as pd
import numpy as np
from pandas.core.frame import DataFrame
import seaborn as sns
import matplotlib.pyplot as plt
import re
import json
from typing import List


"""
===================================================================================
**Cleaner Functions** functions that explore, drop duplicates, rename columns etc.
===================================================================================
"""


def options():
    """Set the options for pandas to display all the columns and rows, and to see the full column width without truncating the result.
    
    .. code-block:: python
       
       def options():
        pd.set_option("display.max_columns", None)
        pd.set_option("max_rows", None)
        # also set an option to see the full column width
        pd.set_option("max_colwidth", None)
        pd.options.mode.chained_assignment = None  # default='warn'

    """

    pd.set_option("display.max_columns", None)
    pd.set_option("max_rows", None)
    # also set an option to see the full column width
    pd.set_option("max_colwidth", None)
    pd.options.mode.chained_assignment = None  # default='warn'


def load_data(columns: str, schema: str, table: str, chunksize=1000, clean=False):
    """Load data from *HANA Cloud* and reads it as pd.DataFrame. 
    
    Loads data in chunks to reduce memory usage for large volume of data. 

    Requires engine connection that should be created before running the `pd.read_sql()`. Uses server-side cursors, aka streaming to load data in chunks faster.

    If clean argument True, while loading clean each chunk by turning empty cells to missing value type `np.nan` and removing prefix from column names, if it's present.


    Args:
        columns (str): names of columns we want to do query of, can also be wild card "*" for all columns.
        schema (str): name of table schema from HANA Cloud.
        table (str): name of the table from HANA Cloud.
        chunksize (int, optional): row count in a single chunk. Defaults to 1000.
        clean (bool, optional): if true data per chunk gets cleaned. Defaults to False.

    Returns:
        pd.DataFrame: all chunks are appended and returned as data frame that can be directly used 

    .. code-block:: python
       
       def load_data(columns: str, schema: str, table: str, chunksize=1000, clean=False):

        # define all the HANA credentials
        address = ("e44640a7-2991-42e8-a4be-2a7d0f348a50.hana.prod-eu20.hanacloud.ondemand.com")
        port = "443"

        # change the json file path as needed
        path_to_json = "./hana_credentials.json"

        # read password and username from the json file
        with open(path_to_json, "r") as credentials:
            info = json.load(credentials)

        user = info["username"]
        password = info["password"]

        # connect to the engine

        engine = create_engine(
            f"hana://{user}:{password}@{address}:{port}",
            connect_args={
                "sslTrustStore": "",
                "encrypt": "true",
                "sslHostNameInCertificate": "*",
                },
            echo=False,
        )
        conn = engine.connect().execution_options(stream_results=True)
        
        # start reading data into chunks
        df_chunk = pd.read_sql_query(
            f'select {columns} from  "{schema}"."{table}"', conn, chunksize=chunksize)  
            
            # set chunk size to 1 million for larger data sets

        # append each chunk df here
        chunk_list = []
        a = 0
        # Each chunk is in df format
        for chunk in df_chunk:
            if clean:
                # clean data
                # - change empty values to NaN
                # - remove column name prefix if present

                chunk_filter = clean_data(chunk)

                # Once the data filtering is done, append the chunk to list
                chunk_list.append(chunk_filter)

        chunk_list.append(chunk)


            a += 1
            print(f"Chunk {a} is ready")

        # concat the list into dataframe
        df = pd.concat(chunk_list)
        # check that the result is a DataFrame
        print(type(df))

        return df

    """
    # define all the HANA credentials
    address = (
        "e44640a7-2991-42e8-a4be-2a7d0f348a50.hana.prod-eu20.hanacloud.ondemand.com"
    )
    port = "443"

    # change the json file path as needed
    path_to_json = "./hana_credentials.json"

    # read password and username from the json file
    with open(path_to_json, "r") as credentials:
        info = json.load(credentials)

    user = info["username"]
    password = info["password"]

    # connect to the engine

    engine = create_engine(
        f"hana://{user}:{password}@{address}:{port}",
        connect_args={
            "sslTrustStore": "",
            "encrypt": "true",
            "sslHostNameInCertificate": "*",
        },
        echo=False,
    )
    conn = engine.connect().execution_options(stream_results=True)

    # read data into chunks
    df_chunk = pd.read_sql_query(
        f'select {columns} from  "{schema}"."{table}"', conn, chunksize=chunksize
    )  # set chunk size to 1 million for larger data sets

    # append each chunk df here
    chunk_list = []
    a = 0
    # Each chunk is in df format
    for chunk in df_chunk:
        if clean:
            # clean data
            # - change empty values to NaN
            # - remove column name prefix if present

            chunk_filter = clean_data(chunk)

            # Once the data filtering is done, append the chunk to list
            chunk_list.append(chunk_filter)

        chunk_list.append(chunk)

        a += 1
        print(f"Chunk {a} is ready")

    # concat the list into dataframe
    df = pd.concat(chunk_list)
    # check that the result is a DataFrame
    print(type(df))

    return df


def clean_data(data: pd.DataFrame) -> pd.DataFrame:
    """Change empty cells to numpy NaN with `np.nan()`. 
    
    Drop the table prefix in column names by calling another custom function `drop_prefix()`, if prefix present.

    
    Args:
        data (pd.DataFrame): data to be cleaned.

    Returns:
        pd.DataFrame: cleaned data frame. 

        .. code-block:: python

            def clean_data(data: pd.DataFrame):
                data_cleaner = drop_prefix(data)
                data_clean = data_cleaner.replace("", np.nan)
                return data_clean


    """
    data_cleaner = drop_prefix(data)
    data_clean = data_cleaner.replace("", np.nan)
    return data_clean


# meet and greet data
def meet_and_greet(data: pd.DataFrame):
    """Meet and great data by printing out shape (number of rows and columns), description and random 5 rows as a sample.

    Args:
        data (pd.DataFrame): data set that we want to explore. ::

            def meet_and_greet(data: pd.DataFrame):
                
                print("Dimension of the table")
                print(data.shape)
                
                print("Sample " + "*" * 50,)
                print(data.sample(5))
                
                print("Description " + "*" * 50,)
                print(data.describe(include="all", datetime_is_numeric=True))

    """
    print("Dimension of the table")
    print(data.shape)
    print("Sample " + "*" * 50,)
    print(data.sample(5))
    print("Description " + "*" * 50,)
    print(data.describe(include="all", datetime_is_numeric=True))


def dictionary_maker(
    dictionary: pd.DataFrame, data: pd.DataFrame, field_name: str
) -> pd.DataFrame:
    """Filter out the dictionary into smaller version to keep only those rows that contain column names for the given data set

    Args:
        dictionary (pd.DataFrame): Dataframe that contains a column names and the descriptive column names as a separate column
        data (pd.DataFrame): The DataFrame based on which dictionary is filtered
        field_name (str): The column name from the dictionary on which the dataframe and dictionary can be mapped
    
    Returns:
           attributes (pd.DataFrame): smaller version of the dictionary with only the relevant content ::

                def dictionary_maker(dictionary: pd.DataFrame, data: pd.DataFrame, field_name: str)->pd.DataFrame:
              
                    # first turn dictionary field names into lower case
                    dictionary["field_name_lower"] = dictionary[field_name].str.lower()

                    # turn data column names into lower case
                    data.columns = data.columns.str.lower()

                    # test that there is no duplicates and NaN left
                    assert dictionary.duplicated().any() == False
                    assert dictionary.isnull().sum().sum() == 0

                    # create attributes data frame with only the relevant columns based on the data
                    filtering = dictionary["field_name_lower"].isin(data.columns)
                    attributes = dictionary[filtering]

                    # test that attributes contains one row per one column in data
                    # if not then print the partially duplicated rows to see the issue
                    if attributes.shape[0] != data.shape[1]:
                        print(attributes[attributes.duplicated(subset="field_name_lower", keep=False)])
                    print(f"attributes dimension is: {attributes.shape}")
                    print(f"data dimension is: {data.shape}")

                    return attributes

    """
    # first turn dictionary field names into lower case
    dictionary["field_name_lower"] = dictionary[field_name].str.lower()

    # turn data column names into lower case
    data.columns = data.columns.str.lower()

    # test that there is no duplicates and NaN left
    assert dictionary.duplicated().any() == False
    assert dictionary.isnull().sum().sum() == 0

    # create attributes data frame with only the relevant columns based on the data
    filtering = dictionary["field_name_lower"].isin(data.columns)
    attributes = dictionary[filtering]

    # test that attributes contains one row per one column in data
    # if not then print the partially duplicated rows to see the issue
    if attributes.shape[0] != data.shape[1]:
        print(attributes[attributes.duplicated(subset="field_name_lower", keep=False)])
    print(f"attributes dimension is: {attributes.shape}")
    print(f"data dimension is: {data.shape}")

    return attributes


def duplicate_reporter(data: pd.DataFrame, by_column: str = None) -> pd.DataFrame:
    """Check for duplicate rows.

    Args:
        data (pd.DataFrame): data set to check.

        by_column (str, optional): column name to subset it by. Defaults to None.

    Returns:
       pd.DataFrame: dataframe of duplicated rows if any, or empty dataframe if no duplicates. 

       .. code-block:: python

           def duplicate_reporter(data: pd.DataFrame, by_column: str = None)-> pd.DataFrame:
                    
                # get the duplicated row index
                duplicates = data.duplicated(subset=by_column, keep=False)
                
                # subset the data by the index
                duplicate_rows = data[duplicates]
                
                return duplicate_rows   

    """

    # get the duplicated row index
    duplicates = data.duplicated(subset=by_column, keep=False)
    # subset the data by the index
    duplicate_rows = data[duplicates]
    return duplicate_rows


def duplicate_na_drop(data: pd.DataFrame) -> pd.DataFrame:
    """Drop completely duplicated records and drop all the missing values  from the DataFrame

    Args:
        data (pd.DataFrame): data set to clean.

    Returns:
        pd.DataFrame: cleaned data set.
        
        .. code-block:: python

            def duplicate_na_drop(data: pd.DataFrame)->pd.DataFrame:
                
                cleaner_df = data.dropna().drop_duplicates()
                
                return cleaner_df

    """
    cleaner_df = data.dropna().drop_duplicates()
    return cleaner_df


def drop_prefix(data: pd.DataFrame) -> pd.DataFrame:
    """ Remove the prefix of the table name such as *table_name + undercore "_"* from the column names, so that it matches to the column names given in the data dictionary.


    Args:
        data (pd.DataFrame): data set
        
    Returns:
        pd.DataFrame: If the prefix exists in the column names it returns the data set with prefixes trimmed. Otherwise, just the old dataset without the change. 

        .. code-block:: python
        
            def drop_prefix(data: pd.DataFrame)-> pd.DataFrame:
                
                for col_name, _ in data.iteritems():
                    
                    # search for the match of the prefix pattern in column names.
                    # pattern if detected, contains any amount of letters and underscore "_"
                    match = re.search(r"^[^_]*_", col_name)
                    if match:
                        prefix = "^[^_]*_"
                        
                        # remove the pattern by replacing it with ""
                        data.columns = data.columns.str.replace(prefix, "", regex=True)
                        return data
                
                return data

    """
    for col_name, _ in data.iteritems():
        # search for the match of the prefix pattern in column names.
        # pattern if detected, contains any amount of letters and underscore "_"
        match = re.search(r"^[^_]*_", col_name)
        if match:
            prefix = "^[^_]*_"
            # remove the pattern by replacing it with ""
            data.columns = data.columns.str.replace(prefix, "", regex=True)
            return data
    return data


def rename_columns(
    data: pd.DataFrame,
    field_name: pd.Series,
    field_description: pd.Series,
    echo: bool = False,
) -> pd.DataFrame:
    """Rename columns of a DataFrame by creating a python dictionary with old:new name pairs. The names must be from the same table presented as 2 columns - current column name and descriptive column name.


    Args:
        data (pd.DataFrame): data set the columns we would like to get renamed
        field_name (pd.Series): old/current names of a DataFrame 
        field_description (pd.Series): new names  taken as pandas series from the dictionary
        echo (bool, optional): if true pandas dictionary with paired names and random rows of renamed dataframe gets printed out. Defaults to False.

    Returns:
        pd.DataFrame: data set with renamed columns; prints out the dictionary with old:new name pairs if it's not more than 100 pairs. 

        .. code-block:: python

            def rename_columns(
                data: pd.DataFrame,
                field_name: pd.Series,
                field_description: pd.Series,
                echo: bool = False,
            )-> pd.DataFrame:

                # make column names lower case to match each other
                data.columns = data.columns.str.lower()
                field_name_lower = field_name.str.lower()

                # create a dictionary from the field name and description as a key value pairs
                names_dict = dict(zip(field_name_lower, field_description))
                
                # use this dictionary to change the names in data with `rename` function
                data_renamed = data.rename(columns=names_dict)
                
                if echo:
                    if len(names_dict) <= 100:
                        print(names_dict)
                    print(data_renamed.sample())
                
                return data_renamed         

    """
    # make column names lower case to match each other
    data.columns = data.columns.str.lower()
    field_name_lower = field_name.str.lower()

    # create a dictionary from the field name and description as a key value pairs
    names_dict = dict(zip(field_name_lower, field_description))

    # use this dictionary to change the names in data with `rename` function
    data_renamed = data.rename(columns=names_dict)
    if echo:
        if len(names_dict) <= 100:
            print(names_dict)
        print(data_renamed.sample())
    return data_renamed


"""
=====================================================================================================
**Completeness:** collection of functions that deal with missing data - assessment of completeness
=====================================================================================================

"""


def fully_missing_columns(data: pd.DataFrame, drop: bool = False) -> List[object]:
    """Iterate over the data frame to find columns with 100% missing values; If indicated with `drop=True` delete these empty columns from the data set.

    Args:
        data (pd.DataFrame): data set to iterate on
        drop (bool): if True drop the empty columns from the data set. Defaults to False.

    Returns:
        * list[object]: A list of names of the columns with 100% missing values. If drop is true then it returns the dataframe `pd.DataFrame` after dropping the empty columns. 
            
            .. code-block:: python

                def fully_missing_columns(data: pd.DataFrame, drop: bool = False)-> List:
                    
                    # iterate over each column and check whether the column contains nothing but NaN (`np.nan` type) 
                    missing = [
                            col_name
                            for col_name, col_value in data.iteritems()
                            if col_value.isnull().sum() == data.shape[0]
                        ]

                        print(f"The total number of fully empty columns is: {len(missing)}")

                        if drop:
                            # for convenience drop these columns from the data set 
                            data_small = data.drop(missing, axis="columns")
                            print(f"The names of empty columns: {missing}")
                            return data_small

                        return missing

    """
    # iterate over each column and
    # check whether the column contains nothing but NaN (`np.nan` type)
    missing = [
        col_name
        for col_name, col_value in data.iteritems()
        if col_value.isnull().sum() == data.shape[0]
    ]

    print(f"The total number of fully empty columns is: {len(missing)}")

    if drop:
        # for convenience drop these columns from the data set
        data_small = data.drop(missing, axis="columns")
        print(f"The names of empty columns: {missing}")
        return data_small

    return missing


def fully_missing_rows(data: pd.DataFrame) -> List:
    """Iterate over the data set to find rows with 100% missing values; checks both types of the missing values NaN and None. 
    
    **Note:** Takes too much time - DO NOT USE FOR LARGE DATA SETS


    Args:
        data (pd.DataFrame): Data set

    Returns:
        list[object]: A list of row index that contain nothing but null values.

        .. code-block:: python
            
            def fully_missing_rows(data: pd.DataFrame)-> List:
                # iterate over each column and
                # check whether the column contains nothing but missing values
                missing = [
                    lab for lab, row in data.iterrows() if row.isnull().sum() == data.shape[1]
                ]
                print(f"The total number of fully empty records is: {len(missing)}")
                
                return missing
    
    """
    # iterate over each column and
    # check whether the column contains nothing but missing values
    missing = [
        lab for lab, row in data.iterrows() if row.isnull().sum() == data.shape[1]
    ]
    print(f"The total number of fully empty records is: {len(missing)}")

    return missing


def calculate_total_missing_percentage(data: pd.DataFrame) -> str:
    """Calculate and print out the total percentage of missing values in the data set. 
    
    **Note:** Checks both types of the missing values NaN and None.


    Args:
        data (pd.DataFrame): data set
    Returns:
        str: prints a sentence about the total percent of missing values.
            
        .. code-block:: python

            def calculate_total_missing_percentage(data: pd.DataFrame)-> str:

                # total number of values
                    total_values = data.shape[0] * data.shape[1]
                    
                    # total number of missing values
                    total_missing = data.isnull().sum().sum()
                    
                    # percent of data that is missing
                    percent = (total_missing / total_values) * 100
                    print("Percent of data that is missing is: " + str(percent))
    
    """

    # total number of values
    total_values = data.shape[0] * data.shape[1]
    # total number of missing values
    total_missing = data.isnull().sum().sum()
    # percent of data that is missing
    percent = (total_missing / total_values) * 100
    print("Percent of data that is missing is: " + str(percent))


def calculate_per_column_percentage_missing(
    data: pd.DataFrame, axis: int = 0
) -> List[object]:
    """Calculate percentage of missing values NaN and/or None for each column or row.
    
     **Note:** empty strings ('') need to be converted to `np.nan` first.


    Args:
        data (pd.DataFrame): Data set
        axis (int, optional): Axis for the function to be applied on - along the rows *0* calculates per column; along the columns *1* calculates per row. Defaults to 0.
                
    Returns:
        list[object]: A list of column names and percent of missing values in descending order 

        .. code-block:: python
                
            def calculate_per_column_percentage_missing(data: pd.DataFrame, axis: int = 0)-> List[object]:
                
                    # Sort out the columns with missing values in descending order
                    missing_percent = data.isnull().mean(axis=axis).sort_values(ascending=False) * 100
                    
                    print("Columns with missing values in descending order")
                    print("*" * 50)
                    
                    print(missing_percent)
                    return missing_percent

    """

    # Sort out the columns with missing values in descending order
    missing_percent = data.isnull().mean(axis=axis).sort_values(ascending=False) * 100

    print("Columns with missing values in descending order")
    print("*" * 50)

    print(missing_percent)

    return missing_percent


def get_complete_columns(data: pd.DataFrame) -> List[object]:
    """Iterate over the data frame to find columns with 100% complete values when nothing is missing.

    Args:
        data (pd.DataFrame): a data set

    Returns:
        object: A list of names of the columns with 100% values present 

        .. code-block:: python
        
            def get_complete_columns(data: pd.DataFrame)-> List[object]:
                
                # check the columns with the zero missing values 
                complete = [
                col_name
                for col_name, col_value in data.iteritems()
                if col_value.isnull().sum() == 0
                ]

                print(f"The total number columns with all the values complete is: {len(complete)}")
                print(complete)
                return complete

    """
    # check the columns with the zero missing values
    complete = [
        col_name
        for col_name, col_value in data.iteritems()
        if col_value.isnull().sum() == 0
    ]
    print(f"The total number columns with all the values complete is: {len(complete)}")
    print(complete)
    return complete


"""Categories: functions that work on columns with category or object data type
===================================================================
"""


def get_categories(data: pd.DataFrame, threshold: int = None) -> pd.DataFrame:
    """Count distinct categories in each column and if less than certain threshold, output only those columns. Does not include missing values as a separate category; 
    
    **Note** If `threshold=1`  the output is the columns with constant values.


    Args:
        data (pd.DataFrame): pandas dataframe
        threshold ([int], optional): distinct category number. Defaults to None.

    Returns:
        pd.DataFrame: data set that contains only the selected columns based on the category number cut off 
    
    .. code-block:: python

        def get_categories(data: pd.DataFrame, threshold: int = None) -> pd.DataFrame:
            
            # get unique categories
            num_unique = data.nunique()
            cols = [col for col, num in zip(data.columns, num_unique) if num <= threshold]
            
            df = data.filter(items=cols)
            return df   

    """

    num_unique = data.nunique()
    cols = [col for col, num in zip(data.columns, num_unique) if num <= threshold]
    df = data.filter(items=cols)
    return df


def count_category_values_per_column(data: pd.DataFrame) -> List[object]:
    """Count values in each category per column and display in descending order.

    Args:
        data (pd.DataFrame): data that contains only the columns with categorical data. The columns can be either category or object data type
    Returns:
        list[object]: record count in each category per column
        
    .. code-block:: python
    
        def count_category_values_per_column(data: pd.DataFrame) -> List[object]:
           
            for _, col in enumerate(data):
                print(data[col].value_counts(sort=True)) 

    """
    for _, col in enumerate(data):
        print(data[col].value_counts(sort=True))


""" Date time data types 
==========================================
"""


def convert_to_datetype(data: pd.DataFrame) -> pd.DataFrame:
    """Convert each column to datetime data type. 
    
    For non consistent dates format - invalid parsing will be set as original data value, without conversion.


    Args:
        data (pd.DataFrame): data set that contains date columns

    Returns:
        pd.DataFrame: the same data set but with column data type of date
    
    .. code-block:: python
    
        def convert_to_datetype(data: pd.DataFrame) -> pd.DataFrame:

            # get columns that contain Date in their names
            pattern = data.columns[data.columns.str.contains("Date", case=False, regex=False)]

            # iterate over the column names in pattern and for each column, change the dtype
            for col in pattern:
                data[col] = pd.to_datetime(
                    data[col], errors="ignore", infer_datetime_format=True
                )  # invalid parsing will be ignored and kept unchanged
            
            return data

    """

    # get columns that contain Date in their names
    pattern = data.columns[data.columns.str.contains("Date", case=False, regex=False)]

    # iterate over the column names in pattern and for each column, change the dtype
    for col in pattern:
        data[col] = pd.to_datetime(
            data[col], errors="ignore", infer_datetime_format=True
        )  # invalid parsing will be ignored and kept unchanged
    return data


def convert_to_time(data: pd.DataFrame) -> pd.DataFrame:
    """Convert each column to time data type; First select only the column that "time" in name; 
    
    
    For non consistent dates format - invalid parsing will be set as original data value, without conversion.


    Args:
        data (pd.DataFrame): data set 
    Returns:
        pd.DataFrame:  data set with columns in the right datetime format    

        .. code-block:: python

            def convert_to_time(data: pd.DataFrame) -> pd.DataFrame:
           
            # get columns that contain time in their names
                pattern = data.columns[data.columns.str.contains("Time", case=False, regex=False)]
                
                # transform each column in the correct time format
                for col in pattern:
                    #invalid parsing will be ignored and kept unchanged
                    data[col] = pd.to_datetime(data[col], format="%H%M%S", errors="ignore").dt.time
                
                return data
           

    """
    # get columns that contain time in their names
    pattern = data.columns[data.columns.str.contains("Time", case=False, regex=False)]

    # transform each column in the correct time format
    for col in pattern:
        # invalid parsing will be ignored and kept unchanged
        data[col] = pd.to_datetime(data[col], format="%H%M%S", errors="ignore").dt.time

    return data

