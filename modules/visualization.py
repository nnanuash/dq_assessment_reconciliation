"""
visualization.py
====================================
The module contains several custom made functions for easier visualizations of completeness and categories per column.




Packages that are used for this module
======================================  

    .. code-block:: python

        import pandas as pd
        import numpy as np
        from pandas.core.frame import DataFrame
        import seaborn as sns
        import matplotlib.pyplot as plt
        from typing import List
        import os

"""

import pandas as pd
import numpy as np
from pandas.core.frame import DataFrame
import seaborn as sns
import matplotlib.pyplot as plt
from typing import List
import os

# the function to save the plot to file.
# Where to save the figures
PROJECT_ROOT_DIR = "."
SUB_FOLDER = "sub_folder"  # a sub-folder
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", SUB_FOLDER)


def save_fig(
    name: str,
    images_path: str = IMAGES_PATH,
    tight_layout: bool = True,
    extension: str = "png",
    resolution: int = 300,
):
    """Save figure after plotting it.

    Args:
        name (str): name of the picture
        images_path (str, optional): path of image directory. Defaults to IMAGES_PATH.
        tight_layout (bool, optional): if true, displays long column names or labels without cutting them short. Defaults to True.
        extension (str, optional): expension of the picture can be .jpg or .svg. Defaults to "png".
        resolution (int, optional): resolution of the picture, the higher the more space it takes in disc. Defaults to 300. ::
            
            def save_fig( name: str, images_path: str = IMAGES_PATH, tight_layout: bool = True, extension: str = "png", resolution: int = 300):
        
                if not os.path.isdir(images_path):
                    os.makedirs(images_path)
                path = os.path.join(images_path, name + "." + extension)
                
                print("Saving figure:", name)
                
                if tight_layout:
                    plt.tight_layout()
                
                plt.savefig(path, format=extension, dpi=resolution) 
                    
    """
    if not os.path.isdir(images_path):
        os.makedirs(images_path)
    path = os.path.join(images_path, name + "." + extension)

    print("Saving figure:", name)

    if tight_layout:
        plt.tight_layout()

    plt.savefig(path, format=extension, dpi=resolution)


def get_missing_value_dataframe(df: pd.DataFrame, index: bool = False) -> pd.DataFrame:
    """ In order to be abe to visualize missing values we need to convert the whole dataframe as percentage of missing and not missing values per column. 
    
    Calculate the percentage of missing values per column and order them in descending order, turn in into a pandas DataFrame with column names as one column, missing percentage another column and add completeness percentage as another column

    NOTE: The code below also contains the optional code example for visualizing the created DataFrame.

    Args:
        df (pd.DataFrame): data set we want to visualize  
        index (bool): Optional; if True, column names set as index because easier to visualize. Defaults to False   

    Returns:
        pd.DataFrame: data that only contains 2 columns with missing and non missing values ( percentage) and column names as a DataFrame index ::
        
        def get_missing_value_dataframe(df: pd.DataFrame, index: bool = False) -> pd.DataFrame:

            # calculate the percentage of missing values per column and order them in descending order
            missing_percent_columns = df.isnull().mean().sort_values(ascending=False) * 100
            
            # turn in into a pandas DataFrame with column names as one column, missing percentage another column and
            #  add completeness persentage as another column
            
            missed_df = pd.DataFrame(
                {
                    "Columns": missing_percent_columns.index,
                    "Missing Values (%)": missing_percent_columns.values,
                    "Non Missing Values (%)": 100 - missing_percent_columns.values,
                }
            )
            
            if index:
                # set column names as index of the data set
                missed_df02 = missed_df.set_index("Columns")
                return missed_df02
            return missed_df

        # After running the function, the created data frame can be used to plot missing values as a stacked bar
        
        # See the code below (optional step)    

        # plotting the missed DataFrame - df
            
        plt.style.use("bmh")
        
        # start figure and axis object
        fig, ax = plt.subplots()
        
        # # plot missing and non missing values on top of each other
        ax.bar(
            df.index, df["Non Missing Values (%)"], label="Non Missing Values (%)",
        )
        ax.bar(
            df.index,
            df["Missing Values (%)"],
            bottom=df["Non Missing Values (%)"],
            label="Missing Values (%)",
        )
        ax.set_yticklabels(df.index)
        ax.set_xlabel("Percent")
        ax.legend(loc="upper right")
        ax.set_title(f"{file}")
        fig.tight_layout()
        plt.show()
       
        # #  plot missing and non missing values horizontally, next to each other
        ax.barh(df.index, df["Non Missing Values (%)"], label="Non Missing Values (%)")
        ax.barh(
            df.index,
            df["Missing Values (%)"],
            left=df["Non Missing Values (%)"],
            label="Missing Values (%)",
        )
        ax.set_yticklabels(df.index)
        ax.set_xlabel("Percent")
        ax.legend(loc="upper right")
        ax.set_title(f"{file}")
        fig.tight_layout()
        plt.show()

    """

    # calculate the percentage of missing values per column and order them in descending order
    missing_percent_columns = df.isnull().mean().sort_values(ascending=False) * 100

    # turn in into a pandas DataFrame with column names as one column, missing percentage another column and
    #  add completeness persentage as another column

    missed_df = pd.DataFrame(
        {
            "Columns": missing_percent_columns.index,
            "Missing Values (%)": missing_percent_columns.values,
            "Non Missing Values (%)": 100 - missing_percent_columns.values,
        }
    )
    if index:
        # set column names as index of the data set
        missed_df02 = missed_df.set_index("Columns")
        return missed_df02
    return missed_df


def get_categories(data: pd.DataFrame, threshold: int = None) -> pd.DataFrame:
    """Count distinct categories in each column and if less than certain threshold, output only those columns. Does not include missing values as a separate category; **Note** If the threshold arg equals 1 then the output is the columns with constant values.
    
    NOTE: The code below contains the optional code example for visualizing the created DataFrame.
    
    Args:
        data (pd.DataFrame): pandas dataframe
        threshold ([int], optional): distinct category number. Defaults to None.

    Returns:
        pd.DataFrame: data set that contains only the selected columns based on the category number cut off 
    
    .. code-block:: python

        def get_categories(data: pd.DataFrame, threshold: int = None) -> pd.DataFrame:
            
            # get unique categories
            num_unique = data.nunique()
            cols = [col for col, num in zip(data.columns, num_unique) if num <= threshold]
            
            df = data.filter(items=cols)
            return df 


         # Plotting categories from the created DataFrame - df

         for i, col in enumerate(df):
            plt.figure(i)
            sns.countplot(y=df[col], data=df, color="c")  

        plt.xticks(rotation=90)
        plt.tight_layout()
        plt.show()
     

    """

    num_unique = data.nunique()
    cols = [col for col, num in zip(data.columns, num_unique) if num <= threshold]
    df = data.filter(items=cols)
    return df
