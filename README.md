**CDO, Information Management Services: DQ Assessment and Reconciliation**

---
More detailed documentation about each of these functions with code examples please visit CDO ShaprePoint for the documentation.

[Documentation](https://asml.sharepoint.com/sites/cdo/Shared%20Documents/Forms/AllItems.aspx?viewid=ee4f7f45%2D0b0b%2D424c%2D803a%2Db7952cbda355&id=%2Fsites%2Fcdo%2FShared%20Documents%2FTrusted%20Data%20Sets%2FData%20Quality%2FDocumentation)
---

**Purpose of the project**

The main purpose of the project is to develop a framework for data quality assessment and reconciliation with the source. In addition, to prototype an approach for the assessment and reconciliation, test it on raw data from HANA Cloud, and write a reusable code in python that will enable automated quality checks and reconciliation when embedded to production.
The quality is checked without the prior knowledge of the business context behind the provided data. Thus, the data quality dimension in scope is completeness and uniqueness. Completeness is how much data is missing from the particular data set and how is the “missingness” distributed per column and record.  Uniqueness is checked by making sure that records are not repeated in a data set and all the columns that include an unique key ( customer or material ID) must have unique values.
Additional (optional) checks are: general data format, dimension, record count in each category per column for the categorical variables, and data type check per column.   

**Core custom-made functions**

The custom made python module contains several files: 

- for general functions and for assessing completeness -  **utils.py** 
- specific to reconciliation - **reconciliation.py** 
- for (optional) visualization of completeness **visualization.py** 

---

**These files contain set of functions that:**

- make easier to load data in chunks (for large data sets)
- 	map and rename the SAP standard column names to more descriptive names
-	assess different aspects of completeness and uniqueness
-	prepare data sets to be comparable
-	and reconcile target with its source.
-	visualize completeness per column and category count for categorical variables
--- 



The module was created with the goal of re-usability and standardization of the process. With these functions the quality checks for completeness as well as the initial pre-processing and loading of the data is standardized and the module can be re-used for every data set with SAP standard format from HANA Cloud. 
The quality checks are not dependent on business context or the content of the data. The functions check only the general aspects of quality such as completeness, data types, category counts etc. Therefore, it can be used as an initial screening and filtering of data to identify issues in advance before creating and/or registering TDS.

---

